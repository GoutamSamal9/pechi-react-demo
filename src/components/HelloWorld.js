import React, { useEffect, useState } from "react";
import "./HelloWorld.css";

const HelloWorld = () => {
  const [text, setText] = useState("Hello World");
  const [stateText, setStateText] = useState("Hello Swayam");
  const [count, setCount] = useState(0);

  const handelClick = () => {
    setText("Hello Pechi");
  };

  const handelMouseOver = () => {
    setStateText("Hello Gelu");
  };

  const handelCount = () => {
    setCount(count + 1);
  };

  useEffect(() => {
    if (count > 0) {
      setStateText("Hello Gelu" + count);
    }
  }, [count]);

  return (
    <>
      <h1>{count}</h1>
      <h1 className="firstHelloWorld">{text}</h1>
      <h1 onMouseOver={handelMouseOver}>{stateText}</h1>

      <button onClick={handelClick}>change text</button>
      <button onClick={handelCount}>Count</button>
    </>
  );
};

export default HelloWorld;
